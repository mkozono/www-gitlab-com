---
layout: markdown_page
title: Software Development Life Cycle
---

## Definition

The modern software development life cycle (SDLC) consists of the following phases. For each phase the appropriate tool is listed.

1. Idea (Chat)
1. Define (Issue Tracker)
1. Plan (Issue Board)
1. Code (Version Control)
1. Test (Continuous Integration)
1. Deploy (Continuous Delivery)
1. Measure (Metrics Monitoring)

## Related concepts

- [Application Lifecycle Management](https://en.wikipedia.org/wiki/Application_lifecycle_management) ALM is defined on wikipedia as "ALM is a broader perspective than the Software Development Life Cycle (SDLC), which is limited to the phases of software development such as requirements, design, coding, testing, configuration, project management, and change management. ALM continues after development until the application is no longer used, and may span many SDLCs."
- [Systems Development Life Cycle](https://en.wikipedia.org/wiki/Systems_development_life_cycle)
- Idea to Production (I2P)

## Methodologies

There are a couple of methodologies that help with the SDLC process:

- Agile
- DevOps
- [Conversational Development](http://conversationaldevelopment.com/) (ConvDev)

## Platforms

You need a platform to run your SDLC on, that is currently undergoing a shift from Virtual Machines to Cloud Native:

- Dedicated machines (Rackspace)
- Virtual Machines (Amazon Web Services)
- Cloud Native (Kubernetes)

## SDLC Stacks

There are a couple of organizations that are building a stack for the SDLC.

https://docs.google.com/spreadsheets/d/1BiFYpwzbiTO6V_PMBIikywX31IZydzPcvBkRwdoVFpQ/pubhtml?gid=0&single=true

TODO Change the above into a markdown table and deprecate the sheet.

## GitLab specific

GitLab is the only Integrated Product / Development OS. We believe that brings emergent benefits.

Features in GitLab related to the SDLC and made possible because of being an integrated product are:

- Chatops
- Review Apps
- Web terminal
- Auto deploy
- Cycle Analytics

TODO Link the above to feature pages / docs.

As a company we believe that [remote only](http://www.remoteonly.org/) will become much more popular.

